.. faas-functions

Working with PiperCI Functions
==============================

PiperCI utilizes OpenFaaS as it's primary worker backend

.. toctree::
   :maxdepth: 1
   :caption: Provided functions in this release
   :glob:

   *-faas/README
   faas-templates/README

Developing Your Own PiperCI Function
------------------------------------

The most basic PiperCI function example is the :doc:`noop-faas </noop-faas/README>` Like it's name implies it does nothing but register itself to GMan and record a simple log. However it give a good basic example on how to interact with the PiperCI python library.

A more complete working example would be the :doc:`Echo </echo-faas/README>` function. This function runs a specified command on behalf of the user and give a more complete example on how a PiperCI function might be implemented.

Also provided are the base :doc:`FaaS function templates </faas-templates/README>`. Basically Docker images that have been prepared to work with PiperCI's API's. These can and should be used as the basis for PiperCI functions.


Deploying Your PiperCI Function
-------------------------------

Write section
